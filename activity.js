db.fruits.insertMany ([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },
  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },
  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },
  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  },
  {
    name : "Papaya",
    color : "Orange",
    stock : 30,
    price: 100,
    supplier_id : 1,
    onSale : false,
    origin: [ "China", "India" ]
  },
  {
    name : "Pitaya",
    color : "Pink",
    stock : 14,
    price: 200,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "US" ]
  },
  {
    name : "Jackfruit",
    color : "Green",
    stock : 12,
    price: 90,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "India" ]
  }
]);

db.fruits.aggregate([
   { $match: { onSale: true } },
   { $count: "fruitsOnSale" }
]);

db.fruits.aggregate([
  { $match: { stock: { $gte: 20 } } },
  { $count: "enoughStock"}
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } }
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } }
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", min_price: { $min: "$price" } } }
